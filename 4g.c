#include <stdio.h>  
#include <termios.h>  
#include <strings.h>  
#include <string.h>  
#include <stdlib.h>  
#include <fcntl.h>  
#include <unistd.h>  

//#define DEbug
int main(int argc, char *argv[]) 
{  
	
	int serial_fd, len, i, singel, singel1;
        char read_buf[100] = { 0 };
	char *ret = NULL;
        char write_buf[7] = { 0x41, 0x54, 0x2b, 0x43, 0x53, 0x51, 0x0d };//"AT+CSQ\n"

    {/* usage */
        if (argc != 2) {
            printf("%s nod  eg:gpstest /dev/ttyUSB0\n", argv[0]);
            exit(-1);
        }
    }//

        serial_fd = open(argv[1] ,O_RDWR | O_NOCTTY | O_NDELAY );
        if(serial_fd < 0)
                printf("4g singel open error\n");
	while(1) {
		tryagain : 
		memset(read_buf, 0, sizeof(read_buf));
                write(serial_fd, write_buf, 7);
                len = read(serial_fd, read_buf, sizeof(read_buf));
		if( len == 12 ) {
#ifdef DEbug
                printf("%s\n len = %d\n", read_buf, len);
#endif
		if ((ret=strstr(read_buf, "+CSQ")) != NULL) {
#ifdef DEbug
                	for(i = 0; i < len; i++)
                	{
                	        printf("read_buf[%d] = %02x  \n", i, read_buf[i]);
                	}
#endif
			sscanf(ret,"+CSQ: %d,%d", &singel, &singel1);
			printf("singel1 strength = %d\n",singel1);
			printf("singel strength = %d\n",singel);
            	} else { 
			printf("no useful AT message, restart\n");
			goto tryagain;
		}
		
		break;
		}
	}
}
